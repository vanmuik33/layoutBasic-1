// $(function() {
//     $('.counter-number').countTo();
// })

var isInViewport = function(elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

$(window).on('load', function(event) {
    $('body').removeClass('preloading');
});

$(document).ready(function() {
    var pos = document.querySelector('#counter');
    window.addEventListener('scroll', function(event) {
        if (isInViewport(pos)) {
            $(function() {
                $('.counter-number').addClass('number-show');
                $('.counter-number').countTo();
            });
        }
    }, false);
});



// $(document).ready(function() {
//  $(window).scroll(function(event) {
//      var pos_body = $('html,body').scrollTop();
//      // console.log(pos_body);
//      if(pos_body>3500){
//          $(function() {
//              $('.counter-number').addClass('number-show');
//             $('.counter-number').countTo();
//         });
//      }

//  });

// });

$(document).ready(function() {
    $('.slick-slider').slick({
        dots: true,
        autoplay: false,
        slidesToScroll: 1,
        accessibility: false,
        arrows: false,
        swipeToSlide: true
    });
});